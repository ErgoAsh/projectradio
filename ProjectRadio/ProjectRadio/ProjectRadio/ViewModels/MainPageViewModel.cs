﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectRadio.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        public DelegateCommand PlayerCommand { get; set; }
        public DelegateCommand NewsCommand { get; set; }
        public DelegateCommand PodcastsCommand { get; set; }
        public DelegateCommand ReportCommand { get; set; }
        public DelegateCommand SocialMediaCommand { get; set; }
        public DelegateCommand ContactCommand { get; set; }        

        private string _title;
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            
        }
    }
}
